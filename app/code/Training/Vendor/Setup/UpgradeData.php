<?php

namespace Training\Vendor\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface {

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            $setup->getConnection()
                ->insert(
                    $setup->getTable('training_vendor_entity'), 
                    [
                        'text' => '1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                    ]
                );
            $setup->getConnection()
                ->insert(
                    $setup->getTable('training_vendor_entity'), 
                    [
                        'text' => '2 - Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                    ]
                );
        }
        
        $setup->endSetup();
    }

}
