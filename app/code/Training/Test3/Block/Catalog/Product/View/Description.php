<?php

namespace Training\Test3\Block\Catalog\Product\View;

class Description extends \Magento\Catalog\Block\Product\View\Description {

    protected function _beforeToHtml() {
        $this->setTemplate('Training_Test3::description.phtml');
        parent::_beforeToHtml();
    }

}
