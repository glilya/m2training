<?php

namespace Training\Test3\Controller\Block;

class Index extends \Magento\Framework\App\Action\Action {

    public function execute() {
        $layout = $this->_view->getLayout();
        $block = $layout->createBlock('Training\Test3\Block\Test');
        $this->getResponse()->appendBody($block->toHtml());
    }

}
