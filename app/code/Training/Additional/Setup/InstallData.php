<?php

namespace Training\Additional\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Setup\CategorySetup as CatalogSetup;
use Magento\Catalog\Setup\CategorySetupFactory as CatalogSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class InstallData implements InstallDataInterface
{

    /**
     * @var CatalogSetupFactory
     */
    private $catalogSetupFactory;

    /**
     * @param CatalogSetupFactoryy $catalogSetupFactory
     */
    public function __construct(CatalogSetupFactory $catalogSetupFactory)
    {
        $this->catalogSetupFactory = $catalogSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);
        $catalogSetup->addAttribute(
                CatalogSetup::CATALOG_PRODUCT_ENTITY_TYPE_ID, 'material', [
            'type' => 'text',
            'label' => 'Material',
            'input' => 'multiselect',
            'required' => false,
            'global' => ScopedAttributeInterface::SCOPE_STORE,
            'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            'attribute_set' => 'Default',
            'option' => [
                'values' => [
                    'Plastic',
                    'Steel',
                    'Glass'
                ]
            ]]
        );
    }

}
