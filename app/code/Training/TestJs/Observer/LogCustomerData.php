<?php

namespace Training\TestJs\Observer;

use Magento\Framework\Event\ObserverInterface;

class LogCustomerData implements ObserverInterface {

    /**
     * @var null|\Psr\Log\LoggerInterface
     */
    protected $_logger = null;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        /** @var $controller Magento\Customer\Controller\Account\CreatePost */
        $controller = $observer->getData('account_controller');
        
        $this->_logger->debug(print_r($controller->getRequest()->getParams(), true));
    }

}
