<?php

namespace Training\Orm\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as CatalogAttribute;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CategorySetupFactory
     */
    private $catalogSetupFactory;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(CategorySetupFactory $categorySetupFactory, CustomerSetupFactory $customerSetupFactory)
    {
        $this->catalogSetupFactory = $categorySetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            /** @var CategorySetup $catalogSetup */
            $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);
            $catalogSetup->addAttribute(Product::ENTITY, 'example_multiselect', [
                'label' => 'Example multiselect',
                'visible_on_front' => 1,
                'required' => 0,
                'global' => CatalogAttribute::SCOPE_STORE,
                'type' => 'text',
                'input' => 'multiselect',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'option' => [
                    'values' => [
                        'Option 1',
                        'Option 2',
                        'Option 3'
                    ],
                ]
            ]);
        }
        if (version_compare($context->getVersion(), '0.1.2') < 0) {
            /** @var CategorySetup $catalogSetup */
            $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);
            $catalogSetup->updateAttribute(
                    Product::ENTITY, 'example_multiselect', [
                'frontend_model' => \Training\Orm\Model\Entity\Attribute\Frontend\HtmlList::class,
                'is_html_allowed_on_front' => 1,
                    ]
            );
        }
        if (version_compare($context->getVersion(), '0.1.3') < 0) {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(
                    Customer::ENTITY, 'priority', [
                'label' => 'Priority',
                'type' => 'int',
                'input' => 'select',
                'source' => \Training\Orm\Model\Entity\Attribute\Source\CustomerPriority::class,
                'required' => 0,
                'system' => 0,
                'position' => 100
                    ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'priority')
                    ->setData('used_in_forms', ['adminhtml_customer'])
                    ->save();
        }
    }

}
