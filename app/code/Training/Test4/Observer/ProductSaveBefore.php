<?php

namespace Training\Test4\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductSaveBefore implements ObserverInterface {

    /**
     * @var null|\Psr\Log\LoggerInterface
     */
    protected $_logger = null;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $product = $observer->getProduct();
        if ($product->isDataChanged()) {
            $this->_logger->debug($product->getId());
            foreach (array_keys($product->getOrigData()) as $field) {
                if ($product->dataHasChangedFor($field)) {
                    $this->_logger->debug($field);
                }
            }
        }
    }

}