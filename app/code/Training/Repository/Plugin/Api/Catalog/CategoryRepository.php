<?php

namespace Training\Repository\Plugin\Api\Catalog;

use Magento\Framework\Api\ExtensionAttributesFactory;
use Training\Repository\Model\CategoryCountriesFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;

class CategoryRepository
{

    /**
     * @var ExtensionAttributesFactory
     */
    protected $extensionAttributesFactory;

    /**
     * @var CategoryCountriesFactory
     */
    protected $categoryCountriesFactory;


    /**
     * @param ExtensionAttributesFactory $extensionAttributesFactory
     * @param CategoryCountriesFactory $categoryCountriesFactory
     */
    public function __construct(
            ExtensionAttributesFactory $extensionAttributesFactory, 
            CategoryCountriesFactory $categoryCountriesFactory
    ) {
        $this->extensionAttributesFactory = $extensionAttributesFactory;
        $this->categoryCountriesFactory = $categoryCountriesFactory;
    }

    /**
     * 
     * @param CategoryRepositoryInterface $subject
     * @param CategoryInterface $category
     * @return CategoryInterface
     */
    public function afterGet(
            CategoryRepositoryInterface $subject, 
            CategoryInterface $category
    ) {
        $category = $this->addCountriesToCategory($category);
        return $category;
    }

    /**
     * @param CategoryInterface $category
     * @return CategoryInterface
     */
    private function addCountriesToCategory(CategoryInterface $category)
    {
        $extensionAttributes = $category->getExtensionAttributes();
        if (empty($extensionAttributes)) {
            $extensionAttributes = $this->extensionAttributesFactory->create('Magento\Catalog\Api\Data\CategoryInterface');
        }

        $categoryCountries = $this->categoryCountriesFactory->create()
                ->getCollection()
                ->addFieldToSelect('country_id')
                ->addFieldToFilter('category_id', ['eq' => $category->getId()]);
        
        $result = [];
        foreach ($categoryCountries->getItems() as $country) {
            $result[] = $country->getCountryId();
        }
        
        $extensionAttributes->setCategoryCountries($result);
        $category->setExtensionAttributes($extensionAttributes);
        return $category;
    }

}
