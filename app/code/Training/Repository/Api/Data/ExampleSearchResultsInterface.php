<?php

namespace Training\Repository\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ExampleSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get examples list.
     *
     * @return \Training\Repository\Api\Data\ExampleInterface[]
     */
    public function getItems();

    /**
     * Set examples list.
     *
     * @param \Training\Repository\Api\Data\ExampleInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null);
}
