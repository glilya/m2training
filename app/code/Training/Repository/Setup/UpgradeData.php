<?php

namespace Training\Repository\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $dbVersion = $context->getVersion();

        if (version_compare($dbVersion, '0.1.1') < 0) {
            $tableName = $setup->getTable('training_repository_example');
            $setup->getConnection()->insertMultiple(
                    $tableName, [
                ['name' => 'Foo'],
                ['name' => 'Bar'],
                ['name' => 'Baz'],
                ['name' => 'Qux'],
                    ]
            );
        }

        if (version_compare($dbVersion, '0.1.2') < 0) {
            $tableName = $setup->getTable('catalog_category_countries');
            $setup->getConnection()->insertMultiple(
                    $tableName, [
                ['category_id' => 3, 'country_id' => 'PL'],
                ['category_id' => 3, 'country_id' => 'FR'],
                ['category_id' => 6, 'country_id' => 'IT'],
                ['category_id' => 6, 'country_id' => 'UA'],
                ['category_id' => 3, 'country_id' => 'US']
                    ]
            );
        }

        $setup->endSetup();
    }

}
