<?php

namespace Training\Repository\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table as DdlTable;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $dbVersion = $context->getVersion();

        if (version_compare($dbVersion, '0.1.2') < 0) {
            $tableName = $setup->getTable('catalog_category_countries');
            
            $ddlTable = $setup->getConnection()->newTable($tableName);
            $ddlTable->addColumn(
                    'category_country_id', DdlTable::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true]
            )->addColumn(
                    'category_id', DdlTable::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false]
            )->addColumn(
                    'country_id', DdlTable::TYPE_TEXT, 2, ['nullable' => false]
            );

            $setup->getConnection()->createTable($ddlTable);
        }

        $setup->endSetup();
    }

}
