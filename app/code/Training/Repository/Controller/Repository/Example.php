<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Training\Repository\Api\ExampleRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SortOrder;

class Example extends Action
{

    /**
     * @var ExampleRepositoryInterface
     */
    private $exampleRepository;
    
     /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    
     /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    
    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param Context $context
     * @param ExampleRepositoryInterface $exampleRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
            Context $context,
            ExampleRepositoryInterface $exampleRepository,
            SearchCriteriaBuilder $searchCriteriaBuilder,
            FilterBuilder $filterBuilder,
            SortOrderBuilder $sortOrderBuilder
    ) {
        $this->exampleRepository = $exampleRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->getResponse()->setHeader('content-type', 'text/plain');
        
        $filters = array_map(function ($name) {
            return $this->filterBuilder
                            ->setConditionType('eq')
                            ->setField('name')
                            ->setValue($name)
                            ->create();
        }, ['Bar', 'Baz', 'Qux']);
        
        $this->searchCriteriaBuilder->addFilters($filters);
        $this->searchCriteriaBuilder->addSortOrder(
                $this->sortOrderBuilder
                        ->setField('example_id')
                        ->setDirection(SortOrder::SORT_ASC)
                        ->create()
        );
        $this->searchCriteriaBuilder->setPageSize(2);
        $this->searchCriteriaBuilder->setCurrentPage(1);

        $examples = $this->exampleRepository->getList(
                        $this->searchCriteriaBuilder->create()
                );
        foreach ($examples->getItems() as $example) {
            $this->getResponse()->appendBody(sprintf(
                            "%s (%d)\n", $example->getName(), $example->getId()
            ));
        }
    }

}
