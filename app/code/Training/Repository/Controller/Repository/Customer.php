<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;

use Magento\Customer\Api\Data\CustomerInterface;

class Customer extends Action
{

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    
    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    public function __construct(
            Context $context, 
            CustomerRepositoryInterface $customerRepository, 
            SearchCriteriaBuilder $searchCriteriaBuilder,
            FilterBuilder $filterBuilder,
            FilterGroupBuilder $filterGroupBuilder
    ) {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    public function execute()
    {
        $this->getResponse()->setHeader('content-type', 'text/plain');
        
        $this->setEmailFilter();
        $this->setNameFilter();
        $customers = $this->getCustomersFromRepository();

        $this->getResponse()->appendBody(sprintf("List contains %s\n\n", get_class($customers[0])));
        foreach ($customers as $customer) {
            $this->outputCustomer($customer);
        }
    }

    /**
     * @return CustomerInterface[]
     */
    private function getCustomersFromRepository()
    {
        $this->searchCriteriaBuilder->setFilterGroups(
                [$this->filterGroupBuilder->create()]
        );
        $criteria = $this->searchCriteriaBuilder->create();
        $customers = $this->customerRepository->getList($criteria);
        return $customers->getItems();
    }
    
    private function setEmailFilter()
    {
        $emailFilter = $this->filterBuilder
                ->setField('email')
                ->setValue('%1@gmail.com')
                ->setConditionType('like')
                ->create();
        $this->filterGroupBuilder->addFilter($emailFilter);
    }
    
     private function setNameFilter()
    {
        $nameFilter = $this->filterBuilder
                ->setField('firstname')
                ->setValue('Test')
                ->setConditionType('eq')
                ->create();
        $this->filterGroupBuilder->addFilter($nameFilter);
    }

    private function outputCustomer(CustomerInterface $customer)
    {
        $this->getResponse()->appendBody(sprintf(
                "\"%s %s\" <%s> (%s)\n", 
                $customer->getFirstname(), 
                $customer->getLastname(), 
                $customer->getEmail(), 
                $customer->getId())
        );
    }

}
