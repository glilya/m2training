<?php

namespace Training\Repository\Model\ResourceModel\CategoryCountries;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'category_countries_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'category_countries_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\Repository\Model\CategoryCountries::class, \Training\Repository\Model\ResourceModel\CategoryCountries::class);
    }

}
