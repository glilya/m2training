<?php

namespace Training\Repository\Model\ResourceModel\Example;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'example_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'example_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\Repository\Model\Example::class, \Training\Repository\Model\ResourceModel\Example::class);
    }

}
