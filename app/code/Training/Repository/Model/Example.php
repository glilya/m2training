<?php

namespace Training\Repository\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Training\Repository\Api\Data\ExampleInterface;

class Example extends AbstractModel implements IdentityInterface, ExampleInterface
{

    const CACHE_TAG = 'training_repository_example';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\Repository\Model\ResourceModel\Example::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_getData('example_id');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setData('example_id', $id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_getData('name');
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setData('name', $name);
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->_getData('created_at');
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData('modified_at', $createdAt);
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->_getData('updated_at');
    }

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdateddAt($updatedAt)
    {
        $this->setData('updated_at', $updatedAt);
    }

}
