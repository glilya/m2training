<?php

namespace Training\Repository\Model;

use Magento\Framework\Model\AbstractModel;

class CategoryCountries extends AbstractModel
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\Repository\Model\ResourceModel\CategoryCountries::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_getData('category_country_id');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setData('category_country_id', $id);
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->_getData('category_id');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setCategoryId($id)
    {
        $this->setData('category_id', $id);
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->_getData('country_id');
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCountryCode($code)
    {
        $this->setData('country_id', $code);
    }

}
