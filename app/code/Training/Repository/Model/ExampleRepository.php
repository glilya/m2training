<?php

namespace Training\Repository\Model;

use Training\Repository\Api\ExampleRepositoryInterface;
use Training\Repository\Api\Data\ExampleInterface;
use Training\Repository\Api\Data\ExampleInterfaceFactory;
use Training\Repository\Api\Data\ExampleSearchResultsInterface;
use Training\Repository\Api\Data\ExampleSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Training\Repository\Model\ResourceModel\Example\Collection as ExampleCollection;
use Magento\Framework\Api\Search\FilterGroup;
use Training\Repository\Model\Example as ExampleModel;

class ExampleRepository implements ExampleRepositoryInterface
{

    /**
     * @var ExampleInterfaceFactory
     */
    private $exampleFactory;

    /**
     * @var ExampleSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @param ExampleInterfaceFactory $exampleFactory
     * @param ExampleSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
            ExampleInterfaceFactory $exampleFactory,
            ExampleSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->exampleFactory = $exampleFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * 
     * @param SearchCriteriaInterface $searchCriteria
     * @return Data\ExampleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var ExampleCollection $collection */
        $collection = $this->exampleFactory->create()->getCollection();

        /** @var ExampleSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        
        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
        $examples = $this->convertCollectionToDataItemsArray($collection);
        
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($examples);
        
        return $searchResults;
    }
    
    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ExampleCollection $collection
     */
    private function applySearchCriteriaToCollection(
            SearchCriteriaInterface $searchCriteria, 
            ExampleCollection $collection
    ) {
        $this->applySearchCriteriaFiltersToCollection(
                $searchCriteria, $collection
        );
        $this->applySearchCriteriaSortOrdersToCollection(
                $searchCriteria, $collection
        );
        $this->applySearchCriteriaPagingToCollection(
                $searchCriteria, $collection
        );
    }
    
    /**
     * @param ExampleCollection $collection
     * @return ExampleInterface[]
     */
    private function convertCollectionToDataItemsArray(ExampleCollection $collection)
    {
        $examples = array_map(function (ExampleModel $example) {
            /** @var ExampleInterface $dataObject */
            $dataObject = $this->exampleFactory->create();
            $dataObject->setId($example->getId());
            $dataObject->setName($example->getName());
            $dataObject->setCreatedAt($example->getCreatedAt());
            $dataObject->setUpdatedAt($example->getUpdatedAt());
            return $dataObject;
        }, $collection->getItems());
        return $examples;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ExampleCollection $collection
     */
    private function applySearchCriteriaFiltersToCollection(
            SearchCriteriaInterface $searchCriteria, 
            ExampleCollection $collection
    ) {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
    }
    
    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ExampleCollection $collection
     */
    private function applySearchCriteriaSortOrdersToCollection(
            SearchCriteriaInterface $searchCriteria, 
            ExampleCollection $collection
    ) {
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                        $sortOrder->getField(), $sortOrder->getDirection()
                );
            }
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ExampleCollection $collection
     */
    private function applySearchCriteriaPagingToCollection(
            SearchCriteriaInterface $searchCriteria, 
            ExampleCollection $collection
    ) {
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }

    /**
     * @param FilterGroup $filterGroup
     * @param ExampleCollection $collection
     */
    private function addFilterGroupToCollection(
            FilterGroup $filterGroup, 
            ExampleCollection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ?
                    $filter->getConditionType() :
                    'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

}
